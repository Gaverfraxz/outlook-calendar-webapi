# Diseño de una Web API integrada con Outlook Calendar en .NET 6.0


*Este proyecto requirió de un alargado periodo de investigación debido a mi poca experiencia con Graph API.*



Tiempo de desarrollo estimado:

- Investigación: 15 horas

- Implementación: 25 horas



## Operaciones permitidas:

Operaciones locales:	



    [POST] 	  Add Event:      Se añade un evento a la base de datos.

	[PUT]	  Update Event:   Se actualiza un evento en la base de datos.

	[GET]	  List Events:    Se listan todos los elementos (que no hayan sido marcados para ser borrados) en la base de datos.	

	[GET]	  Get Event:      Se lista un evento en específico (que no haya sido marcado para ser borrado).

	[DELETE]  Delete Event:   Se marca un evento específico para ser borrado.



Operaciones remotas:



	[POST] Sign-In:	     Se genera un token de autenticación para el usuario de testing. Permite operar a través de Graph API.

	[GET]  Synchronize:  Se sincroniza la base de datos con el calendario de Outlook.	  	



Para simplificar, decidí considerar que un evento tiene las siguientes propiedades:

- Subject:      El título del evento.

- Description:  La descripción del evento.

- Start Date:   La fecha y hora del inicio del evento.

- End Date:     La fecha y hora del final del evento.

- All Day Flag: Indica si el evento sucede durante el dia entero.



Este evento simplificado esta representado por el contrato establecido en la clase DTO. 



## Overview de Clases:

	CalendarController.cs: Clase que contiene el punto de entrada a la API. 

	CalendarService.cs: Middleman entre el controller y los repos. Contiene la lógica del método de sincronización.

	CalendarRepositoryDB: Repositorio de métodos relacionados con la base de datos local. 

	CalendarRepositoryOutlook: Repositorio de métodos relacionados con el Sign-In y el calendario de Outlook.

	

	AbstractEventDTO: Clase abstracta que heredan los DTO de cada operación. 					  					 

	EventDB: Clase que contiene un evento Outlook (Microsoft.Graph.Event) junto con un DatabaseID y un IsDeletedFlag.

	

	Extensions.cs: Clase que contiene extension methods.

	AppConfig.cs: Clase que contiene las propiedades de configuración para la conexión con la DB y Graph API.



##### Nota sobre DTOs:

*Los DTOs especifican el contrato de entrada pertinente a cada operación.\

Exceptuando los DELETE, existe una clase DTO por cada tipo de operación (GET/POST/PUT). \

Esto permite flexibilidad sobre la clase de entrada, y si se desea incrementar la complejidad agregando más parámetros, puede hacerse fácilmente tan solo cambiando los métodos que castean entre DTOs y eventos de Outlook.*





## Base de Datos:

La API funciona sobre una base de datos PostgreSQL.

Los eventos del usuario se guardan sobre la table 'events' en la base de datos. Esta contiene las siguientes columnas:

        

        id (integer):               ID asociada al evento en la base de datos.

	    subject (varchar):          Título asociado al evento.

	    descripcion (varchar):      Descripción asociada al evento.

	    startdate (timestamp):      Fecha y hora inicial del evento.

	    enddate (timestamp) :       Fecha y hora final del evento.

	    alldayflag (bool):          Indica si el evento ocurre durante todo el día.

	    outlookid (varchar):        ID asociada al evento en Outlook.

	    lastmodified (timestamp):   Fecha y hora de la última modificación.

	    deleted (bool):             Indica si el evento está marcado para ser borrado.



Además la base de datos contiene las siguientes funciones:



	createevent:  Crea un evento en la tabla 'events'.

	updateevent:  Actualiza el evento especificado en la tabla 'events'.

	listevents:   Devuelve todos los eventos en la tabla 'events' que no hayan sido marcados para ser borrados.

	getevent:     Devuelve el evento especificado de la base de datos, solo si no fue marcado para ser borrado.

	deleteevent:  Settea el flag 'deleted' del evento especificado.

	deleterow:    Borra la fila especificada de la base de datos.



La cantidad de columnas es baja debido a la simplicidad de nuestro DTO. No permitimos que el usuario ingrese parámetros como ubicación del evento, nivel de urgencia,

etc. Si se deseara poder trabajar sobre eventos más completos, bastaría con actualizar el DTO con los parámetros extra y añadir columnas a nuestra base de datos.





## Métodos importantes:

### CalendarService.Synchronize: 

###### Este método se encarga de sincronizar la base de datos local con el calendario de Outlook.

#####  Overview de la función:

- *Se dice que un evento local tiene un equivalente en Outlook cuando su campo OutlookEvent.Id está asignado.*

- *Se dice que un evento (local o remoto) es más relevante que otro, cuando su fecha de modificación es más reciente.*				

		

        Dada una lista de eventos locales y remotos.

		Itero sobre la lista de eventos locales:

			Tiene mi evento local un equivalente en Outlook?

				Si:

					Veo cuál es más relevante. 

					Es más relevante el evento local?

						Si: 

							Fue mi evento local marcado a ser borrado?

								Si: Entonces borro el evento asociado de Outlook, y borro el evento local de la DB.

								No: Entonces subo el evento local a Outlook.

						No: El evento remoto es más relevante, actualizo el local.												

				No:

					Fue mi evento local marcado a ser borrado?

						Si: Borro el evento de la DB.

						No: Subo el evento a Outlook.		

		Luego de cada iteración remuevo ambos, el evento local, y su equivalente remoto de la lista, si es que existe.

		Termino de iterar.				

		Cualquier evento que haya quedado en la lista de eventos remotos, no existe en la base de datos local. Agrego esos eventos a la base de datos.





### CalendarRepositoryDB.ExecuteSQLFunction:

###### Este método es responsable de utilizar las funciones definidas en la base de datos. Recibe como parámetros el nombre de una función, y los parámetros que toma. Cuando se trata de una operación de tipo GET, se encarga de asignar la table resultante a una lista de EventDB.

*Nota: En este método se abre y se cierra la conexión a la DB cada vez. Esto es porque en el caso de PostgreSQL, estoy manejando un pool de conexiones con pgBouncer, entonces no hay penalidad en performance al abrir y cerrar las conexiones por cada query. Otra opción seria crear la conexión en el constructor de esta clase, dado que es un singleton, y reutilizar esa misma conexión en cada llamado.*

	

### CalendarRepositoryOutlook.SignIn:

###### Genera un Access Token en base a la configuración de autenticación y las credenciales de testing. Requiere que las credenciales pasen por la aplicación, y por lo tanto es un método inseguro de autenticación.

*Nota:  Bajo una versión anterior, este método se encargaba de crear una instancia de ConfidentialClient, mediante la cual generamos un link de login y a través del cual el usuario podía loggearse. Esta autenticación requiere HTTPS, y no fue posible utilizarla en la demo. Se optó entonces por utilizar [una autenticación menos segura](https://learn.microsoft.com/en-us/graph/auth-v2-service?ranMID=43674&ranEAID=FE4O7wtxe6g&ranSiteID=FE4O7wtxe6g-AG.uyz2_Vb3zDQl9oW7ogA&epi=FE4O7wtxe6g-AG.uyz2_Vb3zDQl9oW7ogA&irgwc=1&OCID=AID2200057_aff_7795_1243925&tduid=(ir__duptd9nyrgkfqybbxhyq2k3ksu2xqptyo3epbych00)(7795)(1243925)(FE4O7wtxe6g-AG.uyz2_Vb3zDQl9oW7ogA)()&irclickid=_duptd9nyrgkfqybbxhyq2k3ksu2xqptyo3epbych00) pero posible de implementar en la demo.* 



## Posibles mejoras:

- La API asume que le pasan los tiempos en UTC, y también los devuelve en UTC. Podría adaptarse para considerar la timezone del usuario.

- Para updatear propiedades específicas de un evento, se deben completar todos los campos indicados por el DTO, esencialmente sobreescribiendo el evento entero.

	Puede arreglarse modificando la función update en la DB con COALESCE()

- No está implementado el refresh del Access Token. Se debe hacer el Sign-In de nuevo.

- Actualmente la aplicación solo permite el acceso de un único usuario. Para incorporar el manejo de múltiples usuarios, únicamente se requeriría agregar una columna userID en la base de datos, y cambiar el método de autenticación en el signin para solicitar las credenciales al usuario (Se puede encontrar una implementación anterior de este tipo de autenticación comentado en CalendarRepositoryOutlook.SignIn())

- El método Synchronize causa que un evento local ya sincronizado se vuelva a subir. 

	(Ej: Si se baja un evento X de Outlook en la primer sincronizacion, e inmediatamente se llama otra vez a Synchronize, se detecta que el evento X en la base de datos local fue modificado más recientemente que el evento equivalente en Outlook, y se va a empezar a subir. Puede evitarse esta subida innecesaria añadiendo la columna 'isSync', que trackea si un evento está sincronizado localmente.)

- Solicitar confirmación del usuario antes de sobreescribir un evento.

- Evitar que luego de borrar un evento en la DB se pueda seguir modificando. No causa ningún problema, pero es más prolijo que no ocurra.
