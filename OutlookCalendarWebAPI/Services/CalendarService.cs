
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI.Entities;
using OutlookCalendarWebAPI.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;
using System.Data;
using AuthAPI.Helpers;
using Microsoft.Extensions.Options;

namespace OutlookCalendarWebAPI.Services {
    public class CalendarService : ICalendarService {
        private readonly ICalendarRepositoryDB DBRepository;
        private readonly ICalendarRepositoryOutlook OutlookRepository;
        private readonly AppConfig AppConfig = null;

        public CalendarService(ICalendarRepositoryDB DBRepo, ICalendarRepositoryOutlook OutlookRepo, IOptionsSnapshot<AppConfig> appConfig) {
            this.AppConfig = appConfig.Value; 
            this.DBRepository = DBRepo;
            this.OutlookRepository = OutlookRepo;
        }

        #region User Authentication

        public void SignIn() {
            OutlookRepository.SignIn();
        }

        #endregion

        #region Calendar Actions (Create/Delete/List/Update)
        // Lists all events which are not marked for deletion
        public IEnumerable<ListEventDTO> ListEvents() {
            List<EventDB> requestedEvents = DBRepository.ExecuteSQLFunction("listevents", "");
            requestedEvents.RemoveAll(elem => elem.IsDeleted == true);
            IEnumerable<ListEventDTO> requestedEventsDTO = requestedEvents.Select(elem => elem.ToDTO());
            return requestedEventsDTO;
        }

        ///<inheritdoc />
        public ActionResult<ListEventDTO> GetEvent(int id) {
            List<EventDB> requestedEvents = DBRepository.ExecuteSQLFunction("getevent", id.ToString());
            if (requestedEvents.Any() && !requestedEvents[0].IsDeleted) {
                return requestedEvents.First().ToDTO(); 
            }
            return new NotFoundResult();
        }

        ///<inheritdoc />
        public void CreateEvent(AddEventDTO calEventDTO) {
            // Checks if the AllDayFlag is set, in which case it sets the dates to midnight,
            //  following Graph API standards
            if (calEventDTO.AllDayFlag) { calEventDTO.SetDatesForAllDayEvent(); }
            EventDB calEvent = calEventDTO.ToEventDB();
            
            DBRepository.CreateEvent(calEvent);
        }

        ///<inheritdoc />
        public void UpdateEvent(int id, UpdateEventDTO calEventDTO) {
            // Checks if the AllDayFlag is set, in which case it sets the dates to midnight,
            //  following Graph API standards
            if (calEventDTO.AllDayFlag) { calEventDTO.SetDatesForAllDayEvent(); }
            EventDB updatedEvent = calEventDTO.ToEventDB(id);                           
            DBRepository.UpdateEvent(updatedEvent);            
        }

        ///<inheritdoc />
        public void DeleteEvent(int id) {
            DBRepository.ExecuteSQLFunction("deleteevent", id.ToString());
        }
        #endregion

        #region Synchronization
        ///<inheritdoc />
        public void Synchronize() {

            // Get the local events from the DB and the remote ones from Outlook
            List<Event> remoteEvents = OutlookRepository.GetEvents();
            IEnumerable<EventDB> localEvents = DBRepository.ListEvents();

            // We iterate in reverse through the local event list
            for (int i = localEvents.Count() - 1; i >= 0; i--) {
                EventDB currentLocalEvent = localEvents.ElementAt(i);

                // Check if the local event has an outlook calendar equivalent
                if (currentLocalEvent.OutlookEvent.Id != null) {

                    // It does, so we find the outlook equivalent and check the last modified date
                    int index = remoteEvents.FindIndex(elem => elem.Id == currentLocalEvent.OutlookEvent.Id);
                    try {
                        Event currentRemoteEvent = remoteEvents.ElementAt(index);

                        // If the local event was more recently modified, I update the remote event data with the local event data
                        // Else I update the local event with the remote event data
                        
                        
                        int timeComparison = DateTimeOffset.Compare(
                            (DateTimeOffset)currentLocalEvent.OutlookEvent.LastModifiedDateTime, 
                            (DateTimeOffset)currentRemoteEvent.LastModifiedDateTime
                        );

                        if (timeComparison > 0) {
                            // The local event was more recently modified. We check if it is marked for deletion
                            if (currentLocalEvent.IsDeleted) {
                                // Then we delete from both the local DB and Outlook
                                OutlookRepository.DeleteEvent(currentRemoteEvent.Id);
                                DBRepository.DeleteDBRow(currentLocalEvent.DatabaseId);
                            } 
                            // If the event was not marked for deletion, we use it to update the Outlook calendar
                            else {
                                OutlookRepository.UpdateEvent(currentLocalEvent);
                            }        
                        }
                        // The remote event was more recently modified, we use it to update the local DB
                        else {
                            EventDB updatedEvent = new(currentRemoteEvent, currentLocalEvent.DatabaseId, false);
                            DBRepository.UpdateEvent(updatedEvent);
                        }

                        // We remove the remote event from the list
                        remoteEvents.RemoveAt(index);

                    } catch (Exception) {
                        // Could not find the remote equivalent  assigned to the local event                        
                        // This means the remote event was deleted, thus the local event must also be deleted
                        DBRepository.DeleteDBRow(currentLocalEvent.DatabaseId);
                    }

                    // The event does not exist in outlook. If it is not marked for deletion, we upload it,
                    // and update the local event with the resultant outlookID. Else we completely erase the event from the DB.
                } else {
                    if (!currentLocalEvent.IsDeleted) {
                        Event createdEventFromResponse = OutlookRepository.CreateEvent(currentLocalEvent).Result;
                        currentLocalEvent.OutlookEvent = createdEventFromResponse;
                        DBRepository.UpdateEvent(currentLocalEvent);
                    } else {
                        DBRepository.DeleteDBRow(currentLocalEvent.DatabaseId);
                    }
                }
            }
            // We finished iterating through the local events. Any event remaining on the remote event list
            // does not exist on the local DB, and we must download them.
            foreach (var eventObject in remoteEvents) {
                EventDB newEvent = new(eventObject);
                DBRepository.CreateEvent(newEvent);
            }
        }
        #endregion
    }


}