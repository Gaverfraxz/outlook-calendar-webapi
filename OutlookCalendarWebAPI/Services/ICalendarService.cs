﻿using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;

namespace OutlookCalendarWebAPI.Services {
    public interface ICalendarService {
        /// <summary>
        /// Generates an access token using built-in credentials for testing.
        /// </summary>
        void SignIn();

        /// <summary>
        /// Synchronizes the local database with Outlook Calendar
        /// </summary>
        void Synchronize();

        /// <summary>
        /// Lists all events in the database which are not marked for deletion in DTO form
        /// </summary>
        /// <returns>A IEnumerable&lt;ListEventDTO&gt; containing the events in DTO form</returns>
        IEnumerable<ListEventDTO> ListEvents();

        /// <summary>
        /// Lists all events in the database which are not marked for deletion in DTO form
        /// </summary>
        /// <param name="id">The Id of the event in the database</param>
        /// <returns>An event in DTO form wrapped in an Action class, or an Action&lt;NotFoundResult&gt; if the event could not be found</returns>
        ActionResult<ListEventDTO> GetEvent(int id);

        /// <summary>
        /// Creates a new event on the database
        /// </summary>
        /// <param name="calEvent">DTO the event will be based on</param>
        void CreateEvent(AddEventDTO calEvent);

        /// <summary>
        /// Updates an event stored on the database
        /// </summary>
        /// <param name="calEvent">Contains the event that will be used to update the database</param>
        void UpdateEvent(int id, UpdateEventDTO calEvent);

        /// <summary>
        /// Marks an event stored on the database for deletion. Does not delete the row
        /// </summary>
        /// <param name="id">Id of the event that will be marked for deletion</param>
        void DeleteEvent(int id);
    }
}