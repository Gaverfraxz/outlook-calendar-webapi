﻿using System;
using System.Linq;

namespace AuthAPI.Helpers
{
    public class AppConfig
    {
        public AppConfig() {}
        public DatabaseConfiguration Database { get; set; } = new DatabaseConfiguration();
        public AuthenticationConfiguration Authentication { get; set; } = new AuthenticationConfiguration();
    }

    public class DatabaseConfiguration {
        public string DBHost { get; set; }
        public string DBUsername { get; set; }
        public string DBPassword { get; set; }
        public string DBName { get; set; }
    }

    public class AuthenticationConfiguration {
        public string TestingUser { get; set; }
        public string TestingPassword { get; set; } 
        public string AppID { get; set; }
        public string ClientSecret { get; set; }
        public string TenantID { get; set; }
        public string BaseAuthorityURL { get; set; }
        public string BaseURL { get; set; }
        public string RedirectUri { get; set; }
        public string[] Scopes { get; set; }
    }
}
