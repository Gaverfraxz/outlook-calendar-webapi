@ECHO OFF

title OutlookAPI Deploy

@ECHO *************************************
@ECHO *****     OutlookAPI Deploy     *****
@ECHO *************************************
pause


SET deploy_path=.\OutlookAPIDeploy
SET project_path=..\

:: Borro y recreo el directorio donde vamos a guardar el deploy local
rd %deploy_path% /S /Q
md %deploy_path%

:: genero los deploys locales
dotnet publish %project_path% -c Release /p:PublishProfile=Properties\PublishProfiles\FolderProfile.pubxml -o %deploy_path% || (set failedCommand="Publish" && goto :error)


if errorlevel 1 goto :exit
if errorlevel 0 goto :exit


:error
echo Error in "%~nx0". Command: %failedCommand%. Errorlevel: %errorlevel%.
pause
exit


:exit
echo Deploy finalizado
pause
exit