using OutlookCalendarWebAPI.Entities;
using OutlookCalendarWebAPI.DTOs;
using Microsoft.Graph;
using Microsoft.Graph.Extensions;
using System.Globalization;

namespace OutlookCalendarWebAPI{
    public static class Extensions{
        /// <summary>
        /// Converts the current EventDB object to DTO form
        /// </summary>        
        /// <returns>A DTO object based on the current EventDB</returns>
        public static ListEventDTO ToDTO(this EventDB calEvent){
            return new ListEventDTO {
                Id = calEvent.DatabaseId,                
                Subject = calEvent.OutlookEvent.Subject,
                Description = calEvent.OutlookEvent.Body.Content,
                StartDate = DateTime.ParseExact(calEvent.OutlookEvent.Start.DateTime, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                EndDate = DateTime.ParseExact(calEvent.OutlookEvent.End.DateTime, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture),
                AllDayFlag = calEvent.OutlookEvent.IsAllDay == null ? false : (bool)calEvent.OutlookEvent.IsAllDay,
            };
        }

        /// <summary>
        /// Converts the current DTO to its equivalent EventDB form
        /// </summary>
        /// <returns>An EventDB object based on the current Event DTO</returns>
        public static EventDB ToEventDB(this AddEventDTO addEventDTO) {
            Event outlookEvent = new() {
                Subject = addEventDTO.Subject,
                Body = new ItemBody {
                    Content = addEventDTO.Description,
                },
                Start = new DateTimeTimeZone {
                    DateTime = addEventDTO.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    TimeZone = "UTC"
                },
                End = new DateTimeTimeZone {
                    DateTime = addEventDTO.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    TimeZone = "UTC"
                },
                IsAllDay = addEventDTO.AllDayFlag
            };
            return new EventDB(outlookEvent, -1, false);              
        }

        /// <summary>
        /// Converts the current DTO to its equivalent EventDB form
        /// </summary>
        /// <param name="databaseID">Database id that will be assigned to the Event</param>
        /// <returns>An EventDB object based on the current Event DTO</returns>
        public static EventDB ToEventDB(this UpdateEventDTO updateEventDTO, int databaseID) {
            Event outlookEvent = new() {
                Subject = updateEventDTO.Subject,
                Body = new ItemBody {
                    Content = updateEventDTO.Description,
                },
                Start = new DateTimeTimeZone {
                    DateTime = updateEventDTO.StartDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    TimeZone = "UTC"
                },
                End = new DateTimeTimeZone {
                    DateTime = updateEventDTO.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    TimeZone = "UTC"
                },
                IsAllDay = updateEventDTO.AllDayFlag
            };
            return new EventDB(outlookEvent, databaseID, false);
        }        

        /// <summary>
        /// Converts an EventDB relevant properties to string format for use in SQL
        /// </summary>        
        /// <returns>A string which represents parameters for an SQL function</returns>
        public static string PropertiesToString(this EventDB calEvent){
            // Check if outlookID is null
            string strEndDate = calEvent.OutlookEvent.End.DateTime.ToString();
            string strOutlookID= calEvent.OutlookEvent.Id;
            if (calEvent.OutlookEvent.Id == null) { strOutlookID = "NULL"; } else { strOutlookID = "'" + strOutlookID + "'"; }

            // Build string sequence. Subject and Description use replace to escape single quotes during sql queries.
            string strParam =   $"'{calEvent.OutlookEvent.Subject.Replace("'", "''")}', " +
                                $"'{calEvent.OutlookEvent.Body.Content.Replace("'", "''")}', " +
                                $"'{calEvent.OutlookEvent.Start.DateTime}', " +
                                $"'{calEvent.OutlookEvent.End.DateTime}', " +
                                $"{calEvent.OutlookEvent.IsAllDay}, " +
                                $"{strOutlookID}";
            return strParam;
        }



    }
}