using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;
using Microsoft.Graph;

namespace OutlookCalendarWebAPI.Repositories{
    public interface ICalendarRepositoryOutlook
    {
        /// <summary>
        /// Generates an access token using built-in credentials for testing.
        /// </summary>
        void SignIn();
        
        /// <summary>
        /// Sends a GET action through a GraphServiceClient and requests all the Events in the user Calendar
        /// </summary>
        /// <returns>A list of Calendar Events in the user default calendar</returns>
        List<Event> GetEvents();
        
        /// <summary>
        /// Sends a POST action through a GraphServiceClient and uploads an Event to the user's Calendar
        /// </summary>
        /// <param name="localEvent">Contains the event that will be uploaded</param>
        /// <returns>The created Event with an Outlook ID assigned</returns>
        Task<Event> CreateEvent(EventDB localEvent);
        
        /// <summary>
        /// Sends a PATCH action through a GraphServiceClient and updates an existing event in the Outlook Calendar         
        /// </summary>
        /// <param name="localEvent">Contains the event that will be used to update Outlook Calendar</param>
        void UpdateEvent(EventDB localEvent);
        
        /// <summary>
        /// Sends a DELETE action through a GraphServiceClient and deletes an existing event in the Outlook Calendar
        /// </summary>
        /// <param name="outlookID">ID of the event that will be deleted</param>
        void DeleteEvent(string outlookID);
    }
}