using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;

namespace OutlookCalendarWebAPI.Repositories{
    public interface ICalendarRepositoryDB
    {
        /// <summary>
        /// Lists all events stored in the database which are not marked for deletion
        /// </summary>
        /// <returns>A IEnumerable&lt;EventDB&gt; containing the events</returns>
        IEnumerable<EventDB> ListEvents();
        /// <summary>
        /// Lists a specific event which is not marked for deletion
        /// </summary>
        /// <param name="id">The Id of the event in the database</param>
        /// <returns>A IEnumerable&lt;EventDB&gt; containing the event, or empty if the event could not be found</returns>
        EventDB GetEvent(int id);
        /// <summary>
        /// Creates a new event on the database
        /// </summary>
        /// <param name="calEvent">Contains the event that will be created</param>
        void CreateEvent(EventDB calEvent);
        /// <summary>
        /// Updates an event stored on the database
        /// </summary>
        /// <param name="calEvent">Contains the event that will be used to update the database</param>
        void UpdateEvent(EventDB calEvent);
        /// <summary>
        /// Marks an event stored on the database for deletion. Does not delete the row
        /// </summary>
        /// <param name="id">Id of the event that will be marked for deletion</param>
        void DeleteEvent(int id);
        /// <summary>
        /// Executes a function stored on the database
        /// </summary>
        /// <param name="functionName">The name of the function to be executed</param>
        /// <param name="parameters">The parameters that will be passed to the function</param>
        /// <returns>A List&lt;EventDB&gt; containing the events when using a GET function, or an empty list when not</returns>
        List<EventDB> ExecuteSQLFunction(string functionName, string parameters);
        /// <summary>
        /// Deletes a row from the database
        /// </summary>
        /// <param name="id">Id of the row thta will be deleted</param>
        void DeleteDBRow(int id);
    }
}