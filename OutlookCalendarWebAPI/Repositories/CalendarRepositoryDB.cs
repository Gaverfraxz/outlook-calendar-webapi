using OutlookCalendarWebAPI.Entities;
using Npgsql;
using Microsoft.Graph;
using AuthAPI.Helpers;
using Microsoft.Extensions.Options;
using OutlookCalendarWebAPI.Services;
using System.Xml.Linq;

namespace OutlookCalendarWebAPI.Repositories{
    public class CalendarRepositoryDB : ICalendarRepositoryDB {
        private readonly IConfigurationSection DatabaseConfig;

        //public CalendarRepositoryDB(IOptionsSnapshot<AppConfig> appConfig) {
        public CalendarRepositoryDB(IConfigurationSection dbConfig) {
            this.DatabaseConfig = dbConfig;
        }

        #region Calendar Actions (List/Get/Create/Update/Delete)

        ///<inheritdoc />
        public IEnumerable<EventDB> ListEvents() {
            IEnumerable<EventDB> requestedEvents = ExecuteSQLFunction("listevents", "");
            return requestedEvents;
        }

        ///<inheritdoc />
        public EventDB GetEvent(int id) {
            IEnumerable<EventDB> requestedEvents = ExecuteSQLFunction("getevent", id.ToString());
            return requestedEvents.First();
        }

        ///<inheritdoc />
        public void CreateEvent(EventDB calEvent) {            
            string strParam = calEvent.PropertiesToString();
            ExecuteSQLFunction("createevent", strParam);
        }

        ///<inheritdoc />
        public void UpdateEvent(EventDB calEvent) {
            string strParam = $"{calEvent.DatabaseId}, " + calEvent.PropertiesToString();
            ExecuteSQLFunction("updateevent", strParam);
        }

        ///<inheritdoc />
        public void DeleteEvent(int id) {
            ExecuteSQLFunction("deleteevent", id.ToString());
        }
        #endregion

        #region Database Connection and Query Execution
        /// <summary>
        /// Instantiates a NpgsqlConnection class using the pre-existing database configuration
        /// </summary>
        /// <returns>A new NpgsqlConnection</returns>
        private NpgsqlConnection CreateDBConnection() {
            NpgsqlConnectionStringBuilder builder = new();
            builder.Host = DatabaseConfig.GetValue<string>("DBHost");
            builder.Username = DatabaseConfig.GetValue<string>("DBUsername");
            builder.Password = DatabaseConfig.GetValue<string>("DBPassword");
            builder.Database = DatabaseConfig.GetValue<string>("DBName");
            
            NpgsqlConnection connection = new NpgsqlConnection(builder.ConnectionString);
            return connection;
        }

        ///<inheritdoc />
        public List<EventDB> ExecuteSQLFunction(string functionName, string parameters) {
            // Creating a connexion
            NpgsqlConnection connection = CreateDBConnection();
            connection.Open();

            // Executing a function
            string strCommand = String.Format("select * from {0}({1});", functionName, parameters);
            NpgsqlCommand command = new NpgsqlCommand(strCommand, connection);
            NpgsqlDataReader reader = command.ExecuteReader();

            // Reading return table
            List<EventDB> responseList = new();
            if (functionName == "getevent" || functionName == "listevents") {
                if (reader.HasRows) {
                    while (reader.Read()) {
                        // We try to save the return data in a EventDB object.
                        try {
                            EventDB calEvent = new() {
                                DatabaseId = reader.GetInt32(0),
                                OutlookEvent = new Event {
                                    Subject = reader.GetString(1),
                                    Body = new ItemBody {
                                        Content = reader.GetString(2),
                                    },
                                    Start = new DateTimeTimeZone {
                                        DateTime = reader.GetDateTime(3).ToString("yyyy-MM-ddTHH:mm:ss"),
                                        TimeZone = "UTC"
                                    },
                                    End = new DateTimeTimeZone {
                                        DateTime = reader.GetDateTime(4).ToString("yyyy-MM-ddTHH:mm:ss"),
                                        TimeZone = "UTC"
                                    },
                                    IsAllDay = reader.GetBoolean(5),
                                    Id = reader.IsDBNull(6) ? null : reader.GetString(6),
                                    LastModifiedDateTime = reader.GetDateTime(7)
                                },
                                IsDeleted = reader.GetBoolean(8),
                            };
                            responseList.Add(calEvent);
                        } catch (Exception) {
                            throw;
                        }
                    }
                }
            }
            connection.Close();
            return responseList;
        }
        
        ///<inheritdoc />
        public void DeleteDBRow(int id) {
            ExecuteSQLFunction("deleterow", $"{id}");
        }
        #endregion
    }
}