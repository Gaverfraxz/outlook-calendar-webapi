using OutlookCalendarWebAPI.Entities;
using Microsoft.Identity.Client;
using System.Diagnostics;
using Microsoft.Graph;
using AuthAPI.Helpers;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;

namespace OutlookCalendarWebAPI.Repositories{
    public class CalendarRepositoryOutlook : ICalendarRepositoryOutlook{
        private readonly IConfigurationSection OutlookConfig;

        // MS Graph API Connection Configuration
        private string? AuthorizationCode;
        private string? AccessToken;
        private IConfidentialClientApplication ConfidentialClient;

        //public CalendarRepositoryOutlook(IOptionsSnapshot<AppConfig> appConfig) {
        public CalendarRepositoryOutlook(IConfigurationSection outlookConfig) {
            this.OutlookConfig = outlookConfig;
        }

        #region User Authentication and Token Generation for Graph API Usage
        ///<inheritdoc />
        public async void SignIn() {

            #region Original secure sign-in method. Had to be changed due to the app being hosted on an http server.
            // 
            //
            //string authorityURL = AppConfig.Authentication.BaseAuthorityURL + AppConfig.Authentication.TenantID + "/";
            //string authorityURL = OutlookConfig.GetValue<string>("BaseAuthorityURL") +
            //                      OutlookConfig.GetValue<string>("TenantID") + "/";
            //string endPoint = OutlookConfig.GetValue<string>("BaseURL") + "me";


            //ConfidentialClient = ConfidentialClientApplicationBuilder
            //    .Create(OutlookConfig.GetValue<string>("AppID"))
            //    .WithClientSecret(OutlookConfig.GetValue<string>("ClientSecret"))
            //    .WithAuthority(new Uri(authorityURL))
            //    .WithRedirectUri(OutlookConfig.GetValue<string>("RedirectUri"))
            //    .Build();
            //// Creating the log in URL
            //var authBuilder = ConfidentialClient.GetAuthorizationRequestUrl(OutlookConfig.GetValue<string[]>("Scopes"));
            //Uri authRequestURL = authBuilder.ExecuteAsync().Result;

            //// Open the log in page in browser
            //System.Diagnostics.Process.Start(new ProcessStartInfo(authRequestURL.ToString()) { UseShellExecute = true });

            #endregion

            #region Less secure signin process that works without https
            var values = new Dictionary<string, string>{
                  { "client_id", OutlookConfig.GetValue<string>("AppID") },
                  { "client_secret", OutlookConfig.GetValue<string>("ClientSecret") },
                  { "scope", "Calendars.ReadWrite" },
                  { "username", OutlookConfig.GetValue<string>("TestingUser") },
                  { "password", OutlookConfig.GetValue<string>("TestingPassword") },
                  { "grant_type", "password" }
              };
            HttpClient client = new HttpClient();
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync($"https://login.microsoftonline.com/{OutlookConfig.GetValue<string>("TenantID")}/oauth2/v2.0/token", content);
            var responseString = await response.Content.ReadAsStringAsync();

            AccessToken = JObject.Parse(responseString)["access_token"].ToString();
            #endregion
        }


        /// <summary>
        /// Instantiates a new GraphServiceClient class based on the existing Access Token
        /// </summary>
        /// <returns>A GraphServiceClient which can be used to send requests to the Graph API</returns>
        private GraphServiceClient CreateGraphClient() {
            var authProvider = new DelegateAuthenticationProvider(async (request) => {
                request.Headers.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AccessToken);
            });
            GraphServiceClient graphClient = new(authProvider);
            return graphClient;
        }
        #endregion

        #region Outlook Graph API Actions (List/Create/Update/Delete)
        ///<inheritdoc />
        public List<Event> GetEvents() {            
            GraphServiceClient graphClient = CreateGraphClient();
            var outlookEvents = graphClient.Me.Events
                .Request()
                .Header("prefer", "outlook.body-content-type=\"text\"")
                .GetAsync();
            return outlookEvents.Result.ToList();            
        }

        ///<inheritdoc />
        public async Task<Event> CreateEvent(EventDB localEvent) {
            Event newEvent = localEvent.OutlookEvent;

            GraphServiceClient graphClient = CreateGraphClient();
            var response = await graphClient.Me.Events
                .Request()
                .Header("prefer", "outlook.body-content-type=\"text\"")
                .AddAsync(newEvent);
            return response;
        }

        ///<inheritdoc />
        public void UpdateEvent(EventDB localEvent) {
            Event updatedEvent = localEvent.OutlookEvent;

            GraphServiceClient graphClient = CreateGraphClient();
            Task.Run(async () =>
                await graphClient.Me.Events[$"{localEvent.OutlookEvent.Id}"]
                    .Request()
                    .Header("prefer", "outlook.body-content-type=\"text\"")
                    .Header("prefer", "outlook.timezone=\"UTC\"")
                    .UpdateAsync(updatedEvent)
            );
        }

        ///<inheritdoc />
        public void DeleteEvent(string outlookID) {
            GraphServiceClient graphClient = CreateGraphClient();
            Task.Run(async () =>
                await graphClient.Me.Events[$"{outlookID}"]
                    .Request()
                    .DeleteAsync()
            );            
        }
    }
    #endregion
}