using System;
using Microsoft.Graph;

namespace OutlookCalendarWebAPI.Entities {
    public class EventDB {
        public EventDB() {
            Event newEvent = new(){                
                Start = new DateTimeTimeZone {
                    TimeZone = "UTC"
                },
                End = new DateTimeTimeZone {
                    TimeZone = "UTC"
                }   
            };
            this.OutlookEvent = newEvent;

            this.DatabaseId = -1;
            this.IsDeleted = false;
        }

        public EventDB(Event outlookEvent) {
            this.OutlookEvent = outlookEvent;
            this.DatabaseId = -1;
            this.IsDeleted = false;
        }

        public EventDB(Event outlookEvent, int databaseId, bool isDeleted ) {
            this.OutlookEvent = outlookEvent;
            this.DatabaseId = databaseId;
            this.IsDeleted = isDeleted;
        }

        public Event OutlookEvent { get; set; }
        public int DatabaseId { get; set; }
        public bool IsDeleted { get; set; }
    }
}