using AuthAPI.Helpers;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Graph.ExternalConnectors;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.Services;

WebApplicationBuilder builder = null;
IConfiguration config = null;

builder = WebApplication.CreateBuilder(args);
config = builder.Configuration;
builder.Services.Configure<AppConfig>(config);

// Add services to the container.
builder.Services.AddScoped<ICalendarService, CalendarService>();

// We register our repositories as a singleton.
builder.Services.AddSingleton<ICalendarRepositoryDB>(provider => new CalendarRepositoryDB(config.GetSection("Database")));
builder.Services.AddSingleton<ICalendarRepositoryOutlook>(provider => new CalendarRepositoryOutlook(config.GetSection("Authentication")));

// Enable annotations
builder.Services.AddSwaggerGen(c => {
    c.EnableAnnotations();
    //c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
});

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();


// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
