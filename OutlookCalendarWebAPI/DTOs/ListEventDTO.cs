using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;
using System.ComponentModel;

// Overrides ValidateParameters since the listing of events has no input parameters.
namespace OutlookCalendarWebAPI.DTOs{
    public record ListEventDTO : AbstractEventDTO {
        public int? Id { get; init; }
        public override bool ValidateParameters() {
            return true;
        }
    }
}