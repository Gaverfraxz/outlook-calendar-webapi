using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;

namespace OutlookCalendarWebAPI.DTOs{
    public record UpdateEventDTO : AbstractEventDTO {

    }
}