using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;
using System.Runtime.CompilerServices;

namespace OutlookCalendarWebAPI.DTOs {
    public abstract record AbstractEventDTO{
        public string Subject { get; init; }
        public string Description { get; init; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool AllDayFlag { get; init; }

        // Invalid when subject is empty, and when the event ends before it starts.
        public virtual bool ValidateParameters() {
            bool isValid = true;
            if (Subject == "") {
                isValid = false;
            }
            if (StartDate > EndDate) {
                isValid = false;
            }
            return isValid;
        }
        // When an event has the AllDayFlag, Graph API standard compels us to set the time to midnight.
        public virtual void SetDatesForAllDayEvent() {
            StartDate = new DateTime(
                StartDate.Year, 
                StartDate.Month, 
                StartDate.Day, 
                0, 0, 0 
            );
            // Sets the EndDate.Day to the following day, if the original startDate and endDate were the same day.
            var endDay = EndDate.Day == StartDate.Day ? EndDate.Day + 1 : EndDate.Day;
            EndDate = new DateTime(
                EndDate.Year,
                EndDate.Month,
                endDay,
                0, 0, 0
            );
        }
    }
}