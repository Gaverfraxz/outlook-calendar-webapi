using System;
using System.Linq;
using System.Collections.Generic;
using OutlookCalendarWebAPI.Entities;
using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.Repositories;
using OutlookCalendarWebAPI.DTOs;
using OutlookCalendarWebAPI;

// Uso otro DTO porque mi contrato no necesita tantos parametros para la creacion de un evento
namespace OutlookCalendarWebAPI.DTOs{
    public record AddEventDTO : AbstractEventDTO {
    }
}