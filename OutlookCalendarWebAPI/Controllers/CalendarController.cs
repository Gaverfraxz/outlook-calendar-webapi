using Microsoft.AspNetCore.Mvc;
using OutlookCalendarWebAPI.DTOs;
using Swashbuckle.AspNetCore.Annotations;
using OutlookCalendarWebAPI.Services;
using AuthAPI.Helpers;
using Microsoft.Extensions.Options;
using System.IO;

namespace OutlookCalendarWebAPI.Controllers{
    
    [ApiController]
    [Route("calendar")]
    public class CalendarController : ControllerBase {
        private readonly ICalendarService Service;
        private readonly AppConfig AppConfig = null;

        public CalendarController(ICalendarService service, IOptionsSnapshot<AppConfig> appConfig) {
            this.Service = service;
            this.AppConfig = appConfig.Value;
        }

        /// <summary>
        /// Opens a sign-in page. Must be closed manually
        /// </summary>
        [HttpPost("signin")]
        [SwaggerOperation("Sign-in")]
        public void SignIn() {
           Service.SignIn();
        }


        /// <summary>
        /// Lists all events from the user's local calendar
        /// </summary>
        /// <returns>A status code OK if the events could be listed, and a status code 500 otherwise</returns>
        [HttpGet("events")]
        [SwaggerOperation("List all events")]
        public ActionResult<IEnumerable<ListEventDTO>> ListEvents() {
            try {
               var events = Service.ListEvents();
                return Ok(events);
            } catch (Exception) {
                return StatusCode(500);
            }
        }

        /// <summary>
        /// Lists a specific event from the user's local calendar
        /// </summary>
        /// <param name="id">Id of the event that will be listed</param>
        /// <returns>The event in DTO form, or a status code 404 if the event could not be found</returns>
        [HttpGet("events/{id}")]
        [SwaggerOperation("List a specific event")]
        public ActionResult<ListEventDTO> GetEvent(int id) {
            var calEvent = Service.GetEvent(id);
            if (calEvent is null) { return NotFound(); } else { return calEvent; }
        }

        
        /// <summary>
        /// Creates an event in the user's local calendar
        /// </summary>
        /// <param name="calEventDTO">Event in DTO form</param>
        /// <returns>A status code OK if the DTO was well formed</returns>
        [HttpPost("events")]
        [SwaggerOperation("Add a new event")]
        public ActionResult<ListEventDTO> AddEvent(AddEventDTO calEventDTO) {
            if (calEventDTO.ValidateParameters()) {
                if (calEventDTO.AllDayFlag) { calEventDTO.SetDatesForAllDayEvent(); }
                Service.CreateEvent(calEventDTO);
                return Ok();
            }
            return BadRequest();
        }


        /// <summary>
        /// Updates an event in the user's local calendar
        /// </summary>
        /// <param name="id">Id of the event that will be updated</param> 
        /// <param name="calEventDTO">Event with updated properties in DTO form</param>
        /// <returns>A status code OK if the DTO was well formed</returns>
        [HttpPut("events/{id}")]
        [SwaggerOperation("Update an existing event")]
        public ActionResult<ListEventDTO> UpdateEvent(int id, UpdateEventDTO calEventDTO) {
            if (calEventDTO.ValidateParameters()) {
                if (calEventDTO.AllDayFlag) { calEventDTO.SetDatesForAllDayEvent(); }
                Service.UpdateEvent(id, calEventDTO);
                return Ok();
            }
            return BadRequest();
        }


        /// <summary>
        /// Marks an event for deletion in the user's local calendar
        /// </summary>
        /// <param name="id">Id of the event that will be deleted</param> 
        /// <returns>A status code OK if the event could be deleted, and a status code 500 otherwise</returns>
        [HttpDelete("events/{id}")]
        [SwaggerOperation("Delete an existing event")]
        public ActionResult<ListEventDTO> DeleteEvent(int id) {
            try {
                Service.DeleteEvent(id);
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }                        
        }
        /// <summary>
        /// Synchronizes the user's local calendar with Outlook
        /// </summary>
        /// <returns>A status code OK if the calendar could be synchronized, and a status code 500 otherwise</returns>
        [HttpGet("synchronize")]
        [SwaggerOperation("Synchronize with Outlook")]
        public ActionResult Synchronize() {
            try {
                Service.Synchronize();
                return Ok();
            } catch (Exception) {
                return StatusCode(500);
            }
        }
    }
}